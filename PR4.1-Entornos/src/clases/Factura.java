package clases;

import java.time.LocalDate;

public class Factura {

	
	private String codigoFactura;
	private LocalDate fecha;
	private String nombreProducto;
	private double precioUnidad;
	private int cantidad;
	private Cliente cliente;
	
	public Factura(String codigoFactura, LocalDate fecha, Cliente cliente) {
		
		this.codigoFactura = codigoFactura;
		this.fecha = fecha;
		this.cliente = cliente;
	}
	
	
	public String getCodigoFactura() {
		return codigoFactura;
	}

	public void setCodigoFactura(String codigoFactura) {
		this.codigoFactura = codigoFactura;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public String getNombreProducto() {
		return nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	public double getPrecioUnidad() {
		return precioUnidad;
	}

	public void setPrecioUnidad(double precioUnidad) {
		this.precioUnidad = precioUnidad;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public double calcularPrecioTotal(){
		
		return cantidad*precioUnidad;
		
	}
	
	public String toString() {
		return "C�digo de factura: " + codigoFactura + ", Fecha:" + fecha + ", Nombre del producto:" + nombreProducto + ", Precio Unidad: " + precioUnidad + ", Cantidad: " 
	+ cantidad + ", Cliente:" + cliente;
	}
}

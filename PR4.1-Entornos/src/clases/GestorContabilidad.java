package clases;

import java.time.chrono.ChronoLocalDate;
import java.util.ArrayList;

public class GestorContabilidad {

	private ArrayList<Factura> listaFacturas;
	private ArrayList<Cliente> listaClientes;
	
	public GestorContabilidad() {
		
		listaFacturas = new ArrayList<Factura>();
		listaClientes = new ArrayList<Cliente>();
	}

	public ArrayList<Factura> getListaFacturas() {
		return listaFacturas;
	}

	public ArrayList<Cliente> getListaClientes() {
		return listaClientes;
	}
	
	
	public void setListaFacturas(ArrayList<Factura> listaFacturas) {
		this.listaFacturas = listaFacturas;
	}

	public void setListaClientes(ArrayList<Cliente> listaClientes) {
		this.listaClientes = listaClientes;
	}

	public Cliente buscarCliente(String dni) {
		Cliente aux;
		for (int i=0; i<listaClientes.size(); i++) {
			aux = listaClientes.get(i);
			if (aux.getDni().equals(dni)) {
				return aux;
			}
		}
		return null;
	}
	
	public Factura buscarFactura(String codigo) {
		Factura aux;
		for (int i=0; i<listaFacturas.size(); i++) {
			aux = listaFacturas.get(i);
			if (aux.getCodigoFactura().equals(codigo)) {
				return aux;
			}
		}
		return null;
	}
	
	public void altaCliente(Cliente cliente) {
		//Si su dni no existe
		
		Cliente aux;
		boolean condicion = false;
		for (int i=0; i<listaClientes.size(); i++) {
			aux = listaClientes.get(i);
			if (aux.getDni().equals(cliente.getDni())) {
				condicion = true;
			}
		}
		
		if (condicion == false) {
			listaClientes.add(cliente);
		}
	}
	
	public void crearFactura(Factura factura) {
		Factura aux;
		boolean condicion = false;
		for (int i=0; i<listaFacturas.size(); i++) {
			aux = listaFacturas.get(i);
			if (aux.getCodigoFactura().equals(factura.getCodigoFactura())) {
				condicion = true;
			}
		}
		
		if (condicion == false) {
			listaFacturas.add(factura);
		}
	}
	
	public Cliente clienteMasAntiguo() {
		//Si hay clientes devuelve el que tiene la fecha de alta + antigua
		Cliente antiguo = null;
		Cliente aux;
		
		if (!listaClientes.isEmpty()) {
			for (int i=0; i<listaClientes.size(); i++) {
				aux = listaClientes.get(i);
				if (aux.getFechaAlta().isBefore((ChronoLocalDate) antiguo)) {
					antiguo = aux;
				}
			}
			return antiguo;
		}else {
			return null;
		}
			
	}
	
	public Factura facturaMasCara() {
		Factura cara = null;
		Factura aux;
		//Si hay facturas devuelve la m�s cara
		if (!listaFacturas.isEmpty()) {
			
			for (int i=0; i<listaFacturas.size(); i++) {
				aux = listaFacturas.get(i);
				if (aux.calcularPrecioTotal()>cara.calcularPrecioTotal()) {
					cara = aux;
				}
			}
			return cara;
		}else {
			return null;
		}
		
	}
	
	public float calcularFacturacionAnual(int anno) {
		float dineroTotal = 0;
		Factura aux;
		int compararYear;
		for (int i=0; i<listaFacturas.size(); i++) {
			aux = listaFacturas.get(i);
			compararYear = aux.getFecha().getYear();
			if (compararYear==anno) {
				dineroTotal = (float) (dineroTotal + aux.calcularPrecioTotal());
			}	
		}
		
		return dineroTotal;
	}
	
	public void asignarClienteAFactura(String dni, String codigoFactura) {
		
		Cliente cliente;
		Factura factura;
		
		for (int i=0; i<listaClientes.size();i++) {
			
			cliente = listaClientes.get(i);
			if (cliente.getDni().equals(dni)) {
				
				for (int j=0; j<listaFacturas.size(); j++) {
					
					factura = listaFacturas.get(j);
					
					if (factura.getCodigoFactura().equals(codigoFactura)) {
						
						factura.setCliente(cliente);
						listaFacturas.set(j, factura);
					}
				}
				
			}
		}
	}
	
	public int cantidadFacturasPorCliente(String dni) {
		int cont = 0;
		Factura factura;
		for (int i=0; i<listaFacturas.size(); i++) {
			factura = listaFacturas.get(i);
			if (factura.getCliente().getDni().equals(dni)) {
				cont++;
			}
		}
		
		return cont;
	}
	
	public void eliminarFactura(String codigo) {
		
		Factura factura;
		for (int i=0; i<listaFacturas.size(); i++) {
			factura = listaFacturas.get(i);
			if (factura.getCodigoFactura().equals(codigo)) {
				listaFacturas.remove(i);
				i--;
			}
		}
		
	}
	
	
	public void eliminarCliente(String dni) {
		//S�lo si no tiene facturas
		Cliente cliente;
		for (int i=0; i<listaClientes.size(); i++) {
			cliente = listaClientes.get(i);
			if (cliente.getDni().equals(dni)) {
				
				if (noTieneFacturas(cliente)) {
					listaClientes.remove(i);
					i--;
				}
				
			}
		}
		
	}

	private boolean noTieneFacturas(Cliente cliente) {

		Factura factura;
		for (int i=0; i<listaFacturas.size(); i++) {
			factura = listaFacturas.get(i);
			if (factura.getCliente().equals(cliente)){
				return false;
			}		
		}
		return true;
	}
	
	
	
	
}

package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

public class TestFacturas {
	
	LocalDate ahora = LocalDate.now();
	GestorContabilidad gt = new GestorContabilidad();
	Cliente cliente = new Cliente("Jes�s", "73010256-E", ahora);
	Factura factura = new Factura("12020-1", ahora, cliente);
	Factura factura2 = new Factura("12020-2", ahora, cliente);
	ArrayList<Factura> listaFacturas = gt.getListaFacturas();
	
	@Test
	@Tag("buscarFactura")
	//Buscamos factura con codigo dado de alta y no debe de dar null
	void testBuscarFacturaIntroducida() {
		assertNotNull(gt.buscarFactura("12020-1"));
	}
	
	@Test
	@Tag("buscarFactura")
	//Buscamos factura que no est� creada y debe dar null
	void testBuscarFacturaSinIntroducir() {
		assertNull(gt.buscarFactura("XXXX"));
	}
	
	
	@Test
	@Tag("crearFactura")
	//Al a�adir una factura que no existe, debe a�adirse al arraylist
	void testCrearFactura() {
		gt.crearFactura(factura);
		Factura esperada = factura;
		//Comparamos con la �ltima factura a�adida
		assertEquals(esperada, gt.getListaFacturas().get(gt.getListaFacturas().size()-1));
	}
	
	@Test
	@Tag("crearFactura")
	//Comprobar que no se repiten facturas
	void testComprobarRepeticionFactura() {
		gt.crearFactura(factura);
		gt.crearFactura(factura);
		int esperado = 1;
		assertEquals(esperado, gt.getListaFacturas().size());
		
	}
	
	@Test
	@Tag("facturaMasCara")
	//Comprobar que devuelve null al solicitar factura m�s cara sin haber facturas
	void testFacturaMasCaraSinFactura() {
		assertNull(gt.facturaMasCara());
	}
	
	@Test
	@Tag("facturaMasCara")
	//Comprobar que la factura m�s cara se asigna correctamente
	void testFacturaMasCara() {
		factura.setCantidad(10);
		factura.setPrecioUnidad(10);
		factura2.setCantidad(10);
		factura2.setPrecioUnidad(5);
		gt.facturaMasCara();
		assertEquals(factura, gt.facturaMasCara());	
	}
	
	
	@Test
	@Tag("facturacionAnual")
	//Comprobar facturaci�n anual con a�o v�lido
	void testFacturacionAnualAnnoValido() {
		assertNotNull(gt.calcularFacturacionAnual(2018));
	}
	
	@Test
	@Tag("facturacionAnual")
	//Comprobar facturaci�n anual sin a�o v�lido
	void testFacturacionAnualAnnoInvalido() {
		
		assertNotNull(gt.calcularFacturacionAnual(12010));
		
	}
	
	
	@Test
	@Tag("cantidadFacturasCliente")
	//Comprobar facturas de un cliente v�lido
	void testCantidadFacturasValido() {
		assertNotNull(gt.cantidadFacturasPorCliente("73010256-E"));
	}
	
	@Test
	@Tag("cantidadFacturasCliente")
	//Comprobar facturas de un cliente inv�lido
	void testCantidadFacturasInvalido() {
		
		assertNull(gt.cantidadFacturasPorCliente("73010256-E"));
		
	}
	
	
	@Test
	@Tag("eliminarFactura")
	//Eliminar factura v�lida
	void testEliminarFacturaCorrecta() {
		gt.eliminarFactura("12020-1");
		assertNull(gt.buscarFactura("12020-1"));
	}
	
	@Test
	@Tag("cantidadFacturasCliente")
	//Eliminar factura inv�lida
	void testEliminarFacturaIncorrecta() {
		
		gt.eliminarFactura("120201");
		assertNull(gt.buscarFactura("120201"));
		
	}
	
	
	
}

package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

public class TestClientes {
	
	LocalDate ahora = LocalDate.now();
	LocalDate anterior = LocalDate.of(1995, 1, 1);
	GestorContabilidad gt = new GestorContabilidad();
	Cliente cliente = new Cliente("Jes�s", "73010256-E", ahora);
	Cliente cliente2 = new Cliente("Jes�s", "73010256-E", anterior);
	Factura factura = new Factura("12020-1", ahora, cliente);
	ArrayList<Cliente> listaClientes = gt.getListaClientes();
	ArrayList<Factura> listaFacturas = gt.getListaFacturas();
	
	
	@Test
	@Tag("a�adirCliente")
	//
	void testCrearCliente() {
		gt.altaCliente(cliente);
		Cliente ultimo = gt.getListaClientes().get(gt.getListaClientes().size()-1);
		assertEquals(cliente, ultimo, "Mensaje de control");
	}
	
	@Test
	@Tag("a�adirCliente")
	//Comprobar que no se repiten clientes
	void testComprobarRepeticionClientes() {
		gt.altaCliente(cliente);
		gt.altaCliente(cliente);
		int esperado = 1;
		assertEquals(esperado, gt.getListaClientes().size());
	}
	
	
	@Test
	@Tag("buscarCliente")
	//Buscamos cliente con dni dado de alta y no debe de dar null
	void testBuscarClienteIntroducido() {
		assertNotNull(gt.buscarCliente("73010256-E"));
	}
	
	@Test
	@Tag("buscarCliente")
	//Buscamos cliente que no est� creado y debe dar null
	void testBuscarClienteSinIntroducir() {
		assertNull(gt.buscarCliente("73010256-B"));
	}
	
	
	@Test
	@Tag("clienteMasAntiguo")
	//Comprobar que devuelve el cliente m�s antiguo
	void testClienteAntiguoConClientes() {
		listaClientes.add(cliente);
		listaClientes.add(cliente2);
		gt.setListaClientes(listaClientes);
		assertEquals(cliente2, gt.clienteMasAntiguo(), "Mensaje de control");
	}
	
	@Test
	@Tag("clienteMasAntiguo")
	//Comprobar que si no hay clientes m�s antiguos devuelve null
	void testClienteAntiguoSinClientes() {
		assertNull(gt.clienteMasAntiguo());
	}
	
	
	@Test
	@Tag("asignarClienteAFactura")
	//Comprobar que se asigna un cliente a determinada factura
	void testClienteFactura() {
		gt.asignarClienteAFactura("73010256-E", "12020-1");
		assertEquals(cliente, factura.getCliente(), "mensaje");
	}
	
	@Test
	@Tag("asignarClienteAFactura")
	//Comprobar que no se asigna ning�n cliente si no es v�lido
	void testFacturaClienteFalso() {
		gt.asignarClienteAFactura("73010256E", "12020-1");
		assertNull(factura.getCliente());
		
	}
	
	
	@Test
	@Tag("eliminarCliente")
	//Eliminar cliente v�lido
	void testEliminarClienteCorrecto() {
		
		listaClientes.clear();
		Cliente aux = new Cliente("Jes�s", "73010256-E", ahora);
		listaClientes.add(aux);
		gt.eliminarCliente("73010256-E");
		assertEquals(listaClientes.size(), 0);
			
	}
	
	@Test
	@Tag("eliminarCliente")
	//Eliminar cliente inv�lido
	void testEliminarClienteIncorrecto() {
		
		listaClientes.clear();
		Cliente aux = new Cliente("Jes�s", "73010256-E", ahora);
		listaClientes.add(aux);
		gt.eliminarCliente("73010256-B");
		assertEquals(listaClientes.size(), 0);
		
	}
	
	
}
